$('#product-botton').on('click', function(e) {
    if (this.hash !== '') {
        e.preventDefault();
        const hash = this.hash;
        $('html, body').animate({
                scrollTop: $(hash).offset().top
            },
            800
        );
    }
});
$('#news-botton').on('click', function(e) {
    if (this.hash !== '') {
        e.preventDefault();
        const hash = this.hash;
        $('html, body').animate({
                scrollTop: $(hash).offset().top
            },
            1200
        );
    }
});
$('#contact-botton').on('click', function(e) {
    if (this.hash !== '') {
        e.preventDefault();
        const hash = this.hash;
        $('html, body').animate({
                scrollTop: $(hash).offset().top
            },
            1800
        );
    }
});